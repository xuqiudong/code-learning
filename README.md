# code learning

## 介绍
1. 把零散的代码部分的学习，整理的清晰一些
2. 包括中间件，框架等学习的代码过程记录下来
 

## 分支：[spring cloud alibaba](https://gitee.com/xuqiudong/code-learning/tree/springcloud-alibaba/)

- nacos
  - registry
  - config
- openFeign
- loadBalance
- gateway
 
